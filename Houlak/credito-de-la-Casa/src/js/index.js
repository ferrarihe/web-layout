//Slider1 input moneda

const { init } = require("browser-sync");

function show_slide1_value(x) {
  document.getElementById("input-moneda").value = "$" + x;
  actualizarValorBoxSlider1(x);
}

function restar_dinero() {
  document.getElementById("slider1").value =
    parseInt(document.getElementById("slider1").value) + 500;
  show_slide1_value(document.getElementById("slider1").value);
}

function sumar_dinero() {
  document.getElementById("slider1").value =
    parseInt(document.getElementById("slider1").value) - 500;
  show_slide1_value(document.getElementById("slider1").value);
}

function agregarMonto() {
  var monedaConSigno = document.getElementById("input-moneda").value;
  var monedaSinSigno = monedaConSigno.replace(/\$/g, "");
  document.getElementById("slider1").value = monedaSinSigno;
  actualizarValorBoxSlider1(monedaSinSigno);
  if (monedaSinSigno === monedaConSigno) {
    document.getElementById("input-moneda").value = "$" + monedaSinSigno;
  }
}

function enterInputMoneda(e) {
  if (e.keyCode === 13) {
    agregarMonto();
  }
}

function actualizarValorBoxSlider1(a) {
  var slider1 = document.getElementById("box-slider1");
  var valor = getComputedStyle(slider1).getPropertyValue("--value");
  slider1.style.setProperty("--value", a);
}

//Slider2 input cuota

function show_slide2_value(x) {
  document.getElementById("input-cuota").value = x;
  actualizarValorBoxSlider2(x);
}

function restar_cuota() {
  document.getElementById("slider2").value =
    parseInt(document.getElementById("slider2").value) + 1;
  show_slide2_value(document.getElementById("slider2").value);
}

function sumar_cuota() {
  document.getElementById("slider2").value =
    parseInt(document.getElementById("slider2").value) - 1;
  show_slide2_value(document.getElementById("slider2").value);
}

function agregarCuota() {
  var a = document.getElementById("input-cuota").value;
  document.getElementById("slider2").value = a;
  actualizarValorBoxSlider2(a);
}

function enterInputCuota(e) {
  if (e.keyCode === 13) {
    agregarCuota();
  }
}

function actualizarValorBoxSlider2(a) {
  var slider2 = document.getElementById("box-slider2");
  var valor = getComputedStyle(slider2).getPropertyValue("--value");
  slider2.style.setProperty("--value", a);
}

//Mostrar Cuenta Prepaga

function mostrarCuentaPrepaga() {
  var cuerpo = document.getElementById("cuerpo--slide10");
  var cardCuerpo = document.getElementById("card__cuerpo--slide10");
  var card = document.getElementById("card--slide10");
  var cuentaPrepaga = document.getElementById("cuenta-pregargada");
  var cuentaNueva = document.getElementById("cuenta-nueva");
  cuentaNueva.style.display = "none";

  if (screen.width < 576) {
    cuentaPrepaga.style.display = "block";
    cuerpo.style.gridTemplateRows = "24px 52px 52px 1125px 52px";
    cardCuerpo.style.height = "971px";
  } else {
    cuentaPrepaga.style.display = "block";
    cuerpo.style.gridTemplateRows = "24px 52px 52px 1210px 52px";
    cuerpo.style.height = "1147px";
    cardCuerpo.style.height = "727px";
    card.style.height = "798px";
  }
}

//Mostrar Cuenta Nueva

function mostrarCuentaNueva() {
  var cuerpo = document.getElementById("cuerpo--slide10");
  var cardCuerpo = document.getElementById("card__cuerpo--slide10");
  var card = document.getElementById("card--slide10");
  var cuentaPrepaga = document.getElementById("cuenta-pregargada");
  var cuentaNueva = document.getElementById("cuenta-nueva");
  cuentaPrepaga.style.display = "none";

  if (screen.width < 576) {
    cuentaNueva.style.display = "block";
    cuerpo.style.gridTemplateRows = "51px 52px 52px 1426px 52px";
    cardCuerpo.style.height = "1272px";
  } else {
    cuentaNueva.style.display = "grid";
    cuerpo.style.gridTemplateRows = "51px 52px 52px 1210px 52px";
    cuerpo.style.height = "1266px";
    cardCuerpo.style.height = "902px";
    card.style.height = "973px";
  }
}

//Activar y desactivar botones Cuenta Prepaga y Cuenta Nueva

function activarBtnCuentaPrepaga() {
  var btnCuentaPrepaga = document.getElementById("btnCuentaPrepaga");
  var btnCuentanNueva = document.getElementById("btnCuentaNueva");
  btnCuentanNueva.classList.remove("boton-small--active");
  btnCuentanNueva.classList.add("boton-small--default");
  btnCuentaPrepaga.classList.add("boton-small--active");
}

function activarBtnCuentaNueva() {
  var btnCuentaPrepaga = document.getElementById("btnCuentaPrepaga");
  var btnCuentanNueva = document.getElementById("btnCuentaNueva");
  btnCuentaPrepaga.classList.remove("boton-small--active");
  btnCuentaPrepaga.classList.add("boton-small--default");
  btnCuentanNueva.classList.add("boton-small--active");
}

// Checkbox active

document.getElementById("checkbox-1").onclick = function () {
  var label = document.getElementById("checkbox");
  if (this.checked) {
    label.classList.add("checkbox--active");
  } else {
    label.classList.remove("checkbox--active");
  }
};

function cuentaActive(cuenta) {
  var viejos = document.getElementsByClassName("cuenta--active");
  for (var i = 0; i < viejos.length; i++) {
    viejos[i].className = viejos[i].className.replace(" cuenta--active", "");
  }
  cuenta.className += " cuenta--active";
}
